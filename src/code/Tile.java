package code;

import java.awt.Color;

public class Tile {
	/**	@author kunlin021
	 * This class Tile is suppose to represent each brick in the board
	 * The instance color variable define the identity of the tile, they are:
	 * black color = wall
	 * gray = hall way brick
	 * red = room/door ( door is part of the room)
	 * 
	 * the hall way tile will set its color to black when player stand on it so no one
	 * can stand of pass by the tile
	 * 
	 */
	private Color _color;
	private String roomname;
	
	public Tile (Color c){
	_color = c;
	}
	public Tile (Color c, String s){
		_color = c;
		roomname = s;
	}

	public boolean walkableTile (){
		if (_color.equals(Color.BLACK)){
			System.out.println("can not move to this place");
			return false;
		}
		if (_color.equals(Color.GRAY)){
			return true;
		}
		if (_color.equals(Color.RED)){
			return true;
		}
		return false;
	}
	//tile made avaliable for other player to enter the player leaves
	public void leavingTile(){
		if(_color.equals(Color.BLACK)){
			_color = Color.GRAY;
		}
	}
	// 
	public void enteringTile(){
		if (_color.equals(Color.GRAY)){
			_color = Color.BLACK;
		}
	}
	
	// acessing the color
	public Color getColor(){
		return _color;
	}
	//changing the color
	public void setColor(Color c){
		_color = c;
	}
	//acessing roomname, useful when making suggestion
	public String getRoomname() {
		return roomname;
	}
	public void setRoomname(String s){
		roomname = s;
	}
	
}

package code;

import java.awt.Color;
import java.util.ArrayList;

/**
 * @author kunlin021
 *
 */
public class Board {
	private ArrayList<Card> weaponList;
	private ArrayList<Card> playerList;
	private ArrayList<Card> roomList;
	private ArrayList<Card> cofidentialList;
	private ArrayList<Card> AllList;
	private ArrayList<Player> plist;
	private Model model;
	public Board (){
		model = new Model();
		weaponList = new ArrayList<Card>();
		weaponList.add(new Card("Knife",Color.RED));
		plist = new ArrayList<Player> ();
		//String[] ary = {"Miss Scarlett","Professor Plum","Mrs. Peacock","Reverend Green","Colonel Mustard","Mrs.White"};
		plist.add(new Player (1,1,"Miss Scarlett"));
		plist.add(new Player (2,1,"Professor Plum"));
		plist.add(new Player (3,1,"Mrs. Peacock"));
		plist.add(new Player (4,1,"Reverend Green"));
		plist.add(new Player (5,1,"Colonel Mustard"));
		plist.add(new Player (6,1,"Mrs.White"));
		
	}
	
	/**
	 * the first loop would check from the left ot the person which is 1 int more in the 
	 * array list has the card, once it gets to the size of the array list
	 * it start at zero to continued check
	 * if the player making suggestion is at 0, the second loop will not do anything
	 * 
	 * @author kunlin021
	 * @param s1 Type String one of the element suggested (room, Player, weapon)
	 * @param s2 Type String one of the element suggested (room, Player, weapon)
	 * @param s3 Type String one of the element suggested (room, Player, weapon)
	 * @param x	The position of the next player in the player list
	 * 	that had to answered the suggestion
	 * @return The player that able to prove suggestion is wrong would be return 
	 * and if no one can do that, 
	 */
	public String MakingSuggestion(String s1,String s2, String s3, Player p){
		int x = plist.indexOf(p);
		for (int i=x+1;i<plist.size();i++){
			if(i<6){
			if (plist.get(i).answeringSuggestion(s1, s2, s3)){
				return plist.get(i).getName();
			}
			}
		}

		for (int i=0;i<x;i++){
			if(x!=0){
			if (plist.get(i).answeringSuggestion(s1, s2, s3)){
				return plist.get(i).getName();
			}
			}
		}
		return "Cannot be answered";
	}
	
	public ArrayList<Player> getPlayerlist(){
		return plist;
	}

}

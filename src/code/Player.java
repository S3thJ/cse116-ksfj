package code;

import java.awt.Point;
import java.util.ArrayList;

public class Player {
	/**
	 * @author Kun Lin
	 * Player will be instantiated once the game start and will be given a specific start
	 * location x , y as the location in the board and tile.
	 */
	private Point location;
	private boolean turn;
	private String name;
	private ArrayList<Card> list;
	/**
	 * @author Kun Lin
	 *  the private boolean value 
	 * @param x horizontal position of the player, in relation to tile 2D array x would be the columns
	 * @param y	veritical position of the player, in relation to the tile 2D array y would be the rows
	 */
	public Player (int x, int y){
		location = new Point (x,y);
		turn = false;
	}
	public Player (int x, int y,String s){
		location = new Point (x,y);
		turn = false;
		name =s;
		list = new ArrayList<Card>();
	}
	
	
	public ArrayList<Card> getList(){
		return list;
	}
	public String getName(){
		return name;
	}
	public void setName(String s){
		name = s;
	}
	public Point getPlayerLocation(){
		return location;
	}
	public void setTurn(boolean b){
		turn = b;
	}
	public boolean isPlayerTurn(){
		return turn;
	}
	
	/**
	 * @author kunlin021
	 * @param s1 One of the card name sugguested (Room,Player,Weapon)
	 * @param s2 One of the card name sugguested (Room,Player,Weapon)
	 * @param s3 One of the card name sugguested (Room,Player,Weapon)
	 * @return true if the player has the card, false otherwise 
	 */
	public boolean answeringSuggestion(String s1, String s2, String s3){
		for (int i=0;i<list.size();i++){
			if(list.get(i).getcardName().equals(s1)){
				return true;
			}
			if(list.get(i).getcardName().equals(s2)){
				return true;
			}
			if(list.get(i).getcardName().equals(s3)){
				return true;
			}			
		}
		return false;
	}
	
}

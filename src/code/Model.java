package code;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class Model {
	/**
	 * @author Kun Lin
	 */
	private Tile[][] tileary;
	public static final int COLS = 24;
	public static final int ROWS = 25;
	private Player _player;
	public Model (){
		tileary = new Tile[ROWS][COLS];
		//whole board to gray
		for(int j=0;j<ROWS;j++){
			for(int k=0;k<COLS;k++){
				tileary[j][k] = new Tile(Color.GRAY,"");
			}
		}
		//next 5 for loop set the side
		for(int i=0;i<ROWS;i++){
				tileary[i][0] = new Tile(Color.BLACK,"");	
		}
		for(int i=0;i<ROWS;i++){
			tileary[i][COLS-1] = new Tile(Color.BLACK,"");	
		}
		for(int i=0;i<COLS;i++){
			tileary[0][i] = new Tile(Color.BLACK,"");	
		}
		for(int i=0;i<COLS;i++){
			tileary[ROWS-1][i] = new Tile(Color.BLACK,"");	
		}
		for(int i=0;i<COLS;i++){
			tileary[ROWS-2][i] = new Tile(Color.BLACK,"");	
		}
		//top left room
		for(int j=1;j<4;j++){
			for(int k=1;k<7;k++){
				tileary[j][k].setColor(Color.BLACK);
			}
		}
		//top middle room
		for(int j=1;j<7;j++){
			for(int k=9;k<15;k++){
				tileary[j][k].setColor(Color.BLACK);
			}
		}
		//top right room
		for(int j=1;j<6;j++){
			for(int k=17;k<23;k++){
				tileary[j][k].setColor(Color.BLACK);
			}
		}
		//mid left1 room
		for(int j=6;j<11;j++){//row
			for(int k=1;k<7;k++){//col
				tileary[j][k].setColor(Color.BLACK);
			}
		}
		tileary[6][6].setColor(Color.GRAY);
		tileary[10][6].setColor(Color.GRAY);
		//middle middle confidential file place
			for(int j=8;j<15;j++){
				for(int k=9;k<14;k++){
					tileary[j][k].setColor(Color.BLACK);
				}
			}
		//middle right room
			for(int j=9;j<16;j++){
				for(int k=16;k<23;k++){
					tileary[j][k].setColor(Color.BLACK);
				}
			}	
			tileary[15][16].setColor(Color.GRAY);
			tileary[15][17].setColor(Color.GRAY);
			tileary[15][18].setColor(Color.GRAY);	
		//mid left2 room
			for(int j=12;j<17;j++){//row
				for(int k=1;k<6;k++){//col
					tileary[j][k].setColor(Color.BLACK);
				}
			}
		//bottom left room
			for(int j=19;j<23;j++){//row
				for(int k=1;k<6;k++){//col
					tileary[j][k].setColor(Color.BLACK);
				}
			}
			tileary[19][6].setColor(Color.BLACK);
			
		//bottom mid room
			for(int j=17;j<22;j++){
				for(int k=8;k<16;k++){
					tileary[j][k].setColor(Color.BLACK);
				}
			}
		//bottom right room
			for(int j=18;j<22;j++){
				for(int k=18;k<23;k++){
					tileary[j][k].setColor(Color.BLACK);
				}
			}
			for(int i=7;i<10;i++){
				tileary[23][i].setColor(Color.BLACK);
		}
			for(int i=14;i<17;i++){
				tileary[23][i].setColor(Color.BLACK);
		}
		//setting door/actual room space
		tileary[3][6].setColor(Color.RED);
		tileary[3][6].setRoomname("Study");
		tileary[4][9].setColor(Color.RED);
		tileary[4][9].setRoomname("Hall");
		tileary[6][11].setColor(Color.RED);
		tileary[6][12].setColor(Color.RED);
		tileary[6][11].setRoomname("Hall");
		tileary[6][12].setRoomname("Hall");
		tileary[5][17].setColor(Color.RED);
		tileary[5][17].setRoomname("Lounge");
		tileary[8][6]= new Tile(Color.RED,"Library");
		tileary[10][3]= new Tile(Color.RED,"Library");
		tileary[18][19]= new Tile(Color.RED,"Kitchen");
		tileary[19][4]= new Tile(Color.RED,"Conservatory");
		tileary[5][17]= new Tile(Color.RED,"Lounge");
		//not done but good enought to do the test
		
	}
	
	/**
	 * later on when making suggestion the player being sugguested would had to be
	 * move into the targeted room
	 * @author Kun Lin
	 * @param x 
	 * @param y
	 */
	public void movingPlayerIntoTheRoom (int x, int y){
		_player.getPlayerLocation().setLocation(x,y);
	}
	
	/* a player's turn should be over once he enter a room
	 * 
	 */
	public void EnteringRoom(){
		
	}
	/**
	 * @author kunlin021
	 * this method take in a array list of point that player try to move
	 * and
	 * @param l list of point that indicate where player try to move
	 * @param dicenumber the number that dice rolled
	 * @param p	the player that is trying to make the move
	 * @return true if it is legal moves, false other wise
	 */
	public boolean move(ArrayList<Point> l , int dicenumber,Player p){
		boolean retVal = true;
		int avaliableMove = dicenumber;
		for	(int i=0;i<l.size();i++){
			if(avaliableMove==0){
				return false;
			}
			boolean b = moveOneTileOnly(p,(int)l.get(i).getX(),(int)l.get(i).getY());
			if (b) {avaliableMove -=1;}
			retVal = retVal && b;				
		}
		return retVal;
	}
	/**
	 * @author kunlin021
	 * the method call it self only allow player to move 1 square in 4 direction
	 * up down right left
	 * the player had to choose the player they want to move on, and then the tile player used
	 * to stand will change back to Gray color so other player could enter
	 * the tile player move on to would perhaps set to gray if its hallway tile
	 * if the dice number left is 0, the player won't able to move
	 * for next stage we'll figure out how to decrease the dice number
	 * 	(x,y) is a point indicate where the player try to move to
	 * @param dicenumber 
	 * @param p	Player that try to move
	 * @param x col int
	 * @param y row int
	 * @return	ture if these move is valid, false other wise
	 */
	public boolean moveOneTileOnly (Player p, int x, int y){
		if(p.isPlayerTurn()==false){
			return false;
		}
		int distanceInX = (int) Math.abs(p.getPlayerLocation().getX()-x);
		int distanceInY = (int) Math.abs(p.getPlayerLocation().getY()-y);
		if (distanceInX+distanceInY!=1){
			return false;	
		}
//		if (dicenumber==0){
//			System.out.println("no further avaliable steps");
//			return false;
//		}
		boolean retVal = tileary[y][x].walkableTile();
		if(retVal==true){
			tileary[(int) p.getPlayerLocation().getY()][(int) p.getPlayerLocation().getX()].leavingTile();
			p.getPlayerLocation().setLocation(x, y);
			tileary[y][x].enteringTile();
			if(tileary[y][x].getColor().equals(Color.RED)){
				//TODO entering a room handling
			}
		}
		return retVal;
	}
	
	/**
	 *  * @author kunlin021
	 * taking secret Passage as a move
	 * the passage is only diagonally room from top left only go to bottom right
	 * so given the room name and player that try to make the passage
	 * ill search for the Point (x,y) that is the targeted passage location,and then 
	 * move the player over
	 * 
	 * @param s the room name that player is in
	 * @param p the player that calling the move
	 * @return
	 */
	public boolean takingSecretPassage(String s,Player p){
		//first check make sure the player is in a room
		if(!tileary[(int) p.getPlayerLocation().getY()][(int) p.getPlayerLocation().getX()].getColor().equals(Color.RED)){
			System.out.println("Not in a room");
			return false;
		}
		if (p.isPlayerTurn()==false){
			return false;
		}
		// So far I only created 1 scre
		if(s.equals("Study")){
			for(int j=0;j<ROWS;j++){
				for(int k=0;k<COLS;k++){
					if(tileary[j][k].getRoomname().equals("Kitchen")){
					p.getPlayerLocation().setLocation(k,j);
					p.setTurn(false);
					return true;
					}
				}
			}
		if(s.equals("Kitchen")){
			for(int j=0;j<ROWS;j++){
				for(int k=0;k<COLS;k++){
					if(tileary[j][k].getRoomname().equals("Study")){
					p.getPlayerLocation().setLocation(k,j);
					p.setTurn(false);
					return true;
					}
					}
				}
			}
		if(s.equals("Lounge")){
			for(int j=0;j<ROWS;j++){
				for(int k=0;k<COLS;k++){
					if(tileary[j][k].getRoomname().equals("Conservatory")){
					p.getPlayerLocation().setLocation(k,j);
					p.setTurn(false);
					return true;
					}
					}
				}
			}
		}
		if(s.equals("Conservatory")){
			for(int j=0;j<ROWS;j++){
				for(int k=0;k<COLS;k++){
					if(tileary[j][k].getRoomname().equals("Lounge")){
					p.getPlayerLocation().setLocation(k,j);
					p.setTurn(false);
					return true;
					}
					}
				}
			}
		System.out.println("This room don't have secret passage");
		return false;
	}
	/*new idea, at the beginning of the player's turn, if he is in a room, he will have the choice of 
	*selecting which door he want to go out// or selecting a secret passage
	*/
	
	public int rollDice(){
		Random r = new Random();
		return r.nextInt(6) +1;
	}
	public Tile getTile(int x, int y){
		return tileary[y][x];
	}
}

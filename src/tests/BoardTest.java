package tests;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import code.Board;
import code.Card;
import code.Player;

public class BoardTest {
	private Board _board;
	public BoardTest(){
		_board = new Board();
	}
	/**
	 * @author kunlin021
	 * the _board already have a Arraylist of player when instantiated
	 * and there is getPlayerList method in board class that you can get the list
	 * then each Player himself has a arraylist of card which represent the card 
	 * they're holding
	 * 
	 */
	//User story2 test case 1
	@Test public void nextPlayerHasWeaponCard(){
		_board.getPlayerlist().get(3).getList().add(new Card("Weapon",null));
		String s = "Reverend Green";//playerlist.get(3) name
		assertEquals(s,_board.MakingSuggestion("Weapon", "", "", _board.getPlayerlist().get(2)));
	}
	//User story2 test case 2
	@Test public void nextPlayerHasRoomCard(){
		_board.getPlayerlist().get(3).getList().add(new Card("Lounge",null));
		String s = "Reverend Green";//playerlist.get(3) name
		assertEquals(s,_board.MakingSuggestion("Weapon", "Lounge", "", _board.getPlayerlist().get(2)));
	}
	//User story2 test case 3
	@Test public void nextPlayerHasPlayerCard(){
		_board.getPlayerlist().get(3).getList().add(new Card("Mrs.White",null));
		String s = "Reverend Green";//playerlist.get(3) name
		assertEquals(s,_board.MakingSuggestion("Weapon", "Mrs.White", "", _board.getPlayerlist().get(2)));
	}
	//User story2 test case 4
	@Test public void nextPlayerHasTwoMatchingCard(){
		_board.getPlayerlist().get(3).getList().add(new Card("Mrs.White",null));
		_board.getPlayerlist().get(3).getList().add(new Card("Lounge",null));
		String s = "Reverend Green";//playerlist.get(3) name
		assertEquals(s,_board.MakingSuggestion("Weapon", "Mrs.White", "Lounge", _board.getPlayerlist().get(2)));
	}
	//User story2 test case 5
	@Test public void nextNextPlayerHasMatchingCard(){
		_board.getPlayerlist().get(3).getList().add(new Card("Mrs.White",null));
		_board.getPlayerlist().get(3).getList().add(new Card("Kitchen",null));
		_board.getPlayerlist().get(4).getList().add(new Card("Lounge",null));
		String s = "Colonel Mustard";//playerlist.get(3) name
		assertEquals(s,_board.MakingSuggestion("Mouse", "Mr.Kun", "Lounge", _board.getPlayerlist().get(2)));
	}
	//User story2 test case 6
		@Test public void TheLastPlayerHasMatchingCard(){
			_board.getPlayerlist().get(3).getList().add(new Card("Mrs.White",null));
			_board.getPlayerlist().get(3).getList().add(new Card("Kitchen",null));
			//the last player 1 before 2 in the player arraylist, which is "Professor Plum"
			_board.getPlayerlist().get(1).getList().add(new Card("Lounge",null));
			String s = "Professor Plum";//playerlist.get(1) name
			assertEquals(s,_board.MakingSuggestion("Mouse", "Mr.Kun", "Lounge", _board.getPlayerlist().get(2)));
		}
	//User story2 test case 7
		@Test public void sugguestionNotAnsweredButPlayerHasMatchingCard(){
			_board.getPlayerlist().get(3).getList().add(new Card("Mrs.White",null));
			_board.getPlayerlist().get(3).getList().add(new Card("Kitchen",null));
			_board.getPlayerlist().get(4).getList().add(new Card("Lounge",null));
			//play making suggestion has the card
			_board.getPlayerlist().get(2).getList().add(new Card("Library",null));
			String s = "Cannot be answered";//String return when suggestion not answered
			assertEquals(s,_board.MakingSuggestion("Mouse", "Mr.Kun", "Library", _board.getPlayerlist().get(2)));
		}
	//User story2 test case 8
		@Test public void sugguestionNotAnsweredAndPlayerHasNoMatchingCard(){
			_board.getPlayerlist().get(3).getList().add(new Card("Mrs.White",null));
			_board.getPlayerlist().get(3).getList().add(new Card("Kitchen",null));
			_board.getPlayerlist().get(4).getList().add(new Card("Lounge",null));
			//play making suggestion has the card
			_board.getPlayerlist().get(2).getList().add(new Card("Library",null));
			String s = "Cannot be answered";//String return when suggestion not answered
			assertEquals(s,_board.MakingSuggestion("Mouse", "Mr.Kun", "ItWorked!", _board.getPlayerlist().get(2)));
		}
	

}

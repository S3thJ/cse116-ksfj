package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import code.*;

	/**
	 * @author kunlin021
	 *
	 */
public class PlayerTest {

	public PlayerTest(){
		
	}
	@Test public void provingSuggestionWrong(){
		Player p = new Player (1,1,"Mrs.White");
		p.getList().add(new Card("Weapon",null));
		assertFalse(p.answeringSuggestion("", "", "nope"));
		assertTrue(p.answeringSuggestion("", "", "Weapon"));
	}
}

package tests;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import code.Model;
import code.Player;

public class ModelClassTest {
	/**
	 * http://www.wikihow.com/Play-Cluedo/Clue  <<board model based upon on
	 * for this test class
	 * the dice number are decremented by giving a decremented dice input as each
	 * time player call the moving method since Professor Hertz said for now we're 
	 * just going to provide a input instead of an actual dice
	 * for future stage I'll implement a method that would reduce the dice number
	 * 
	 * @author Kun Lin
	 * userstory1: 9 test cases
	 */
	private Model m;
	private ArrayList<Point> list;
	private int dicenumber;
	public ModelClassTest(){
		m = new Model();
		list = new ArrayList<Point>();

	}
		/**
		 * user story1 test case 1
		 */
	@Test public void movingHorizontal(){
		Player p = new Player (7,7);
		//set the player Turn status to true to indicate they're playing
		p.setTurn(true);
		//moving to right horizontally
		list.add(new Point(8,7));
		list.add(new Point(9,7));
		list.add(new Point(10,7));
		dicenumber = 3;
		assertTrue(m.move(list, 3, p));
		Player p2 = new Player (10,7);
		p2.setTurn(true);
		//moving to the left
		list.clear();
		list.add(new Point(9,7));
		list.add(new Point(8,7));
		list.add(new Point(7,7));
		assertTrue(m.move(list, dicenumber, p2));
	}

	
		/**
		 * user story1 test case 2
		 */
	@Test public void movingVertical(){
		Player p = new Player (7,7);
		p.setTurn(true);
		//moving down
		dicenumber = 3;
		list.add(new Point(7,8));
		list.add(new Point(7,9));
		list.add(new Point(7,10));
		assertTrue(m.move(list, dicenumber, p));
		//moving up
		Player p2 = new Player (14,9);
		p2.setTurn(true);
		list.clear();
		list.add(new Point(14,8));
		list.add(new Point(14,7));
		assertTrue(m.move(list, dicenumber, p2));
	}
	/**
	 * user story1 test case 3
	 */
	@Test public void movingBothHorAndVer(){
		Player p2 = new Player (14,9);
		p2.setTurn(true);
		//moving ver 2 square
		list.add(new Point(14,8));
		list.add(new Point(14,7));
		//moving hori 2 square
		list.add(new Point(13,7));
		list.add(new Point(12,7));
		list.add(new Point(11,7));
		int dicenumber =5;
		assertTrue(m.move(list, dicenumber, p2));
	}
	/**
	 * user story1 test case 4
	 */
	@Test public void movingIntoRoom(){
		Player p = new Player (14,7);
		p.setTurn(true);
		//moving hori 2 square
		list.add(new Point(13,7));
		list.add(new Point(12,7));
		//movingintotheroom, where location (12,6) is a room
		list.add(new Point(12,6));
		dicenumber =3;
		assertTrue(m.move(list, dicenumber, p));
	}
	/**
	 * user story1 test case 5
	 */
	@Test public void takingSecretPassageAsMove(){
		Player p = new Player(6,3);
		p.setTurn(true);
		//getting the roomname where the player is in
		String s = m.getTile(6, 3).getRoomname();
		m.takingSecretPassage(s, p);
		//expected teleported room name is "Kitchen"
		//actual == newRoomName
		String newRoomName = m.getTile((int)p.getPlayerLocation().getX(),(int) p.getPlayerLocation().getY()).getRoomname();
		assertEquals(newRoomName,"Kitchen");
		assertFalse(p.isPlayerTurn());
	}
	/**
	 * user story1 test case 6
	 */
		@Test public void tooManyStepThanDiceNumber(){
			Player p = new Player (7,7);
			p.setTurn(true);
			list.add(new Point(7,8));
			list.add(new Point(7,9));
			list.add(new Point(7,10));
			list.add(new Point(7,11));
			dicenumber = 3;
			//4move only dicenumber of 3 expected false
			assertFalse(m.move(list, dicenumber, p));
		}
		/**
		 * user story1 test case 7
		 */
	@Test public void cannotMoveDiagonally(){
		Player p = new Player (14,7);
		p.setTurn(true);
		assertFalse(m.moveOneTileOnly(p,15,8 ));
		Player p2 = new Player(7,7);
		p2.setTurn(true);
		list.add(new Point(8,8));
		list.add(new Point(8,9));
		dicenumber = 2;
		assertFalse(m.move(list, dicenumber, p2));
		
	}
	/**
	 * user story1 test case 8
	 */
	@Test public void NotAContigugousMove(){
		Player p = new Player (14,7);
		p.setTurn(true);
		assertFalse(m.moveOneTileOnly(p,16,7 ));
		Player p2 = new Player(7,7);
		p2.setTurn(true);
		list.add(new Point(7,9));
		dicenumber = 6;
		assertFalse(m.move(list,dicenumber,p2));
	}
	/**
	 * user story1 test case 9, also there is a(walkable tile) test in TileClasstest test class
	 */
	@Test public void cannotGoIntoWall(){
		Player p = new Player (14,7);
		p.setTurn(true);
		assertFalse(m.moveOneTileOnly(p,14,6 ));
		Player p2 = new Player (14,8);
		p2.setTurn(true);
		list.add(new Point(13,8));
		dicenumber =4;
	    assertFalse(m.move(list, dicenumber, p2));
	}
	
	/**
	 * other test:n moving one tile
	 */
	@Test public void moveOneTileTest(){
		Player p = new Player (14,7);
		p.setTurn(true);
		assertTrue(m.moveOneTileOnly(p, 13, 7));
	}

	
}

package tests;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import code.Tile;

public class TileClassTest {
	
	public TileClassTest(){
		
	}
	
	/**
	 * @author Kun Lin
	 * the wall tile and hallway tile that player stands on has different color-
	 * status, which other player cannot enter to that tile
	 *cannot walking into wall
	 */
	@Test public void tilewalkableCheck(){
			Tile t = new Tile(Color.BLACK,"");
			//cannot walk into wall/player
			assertFalse(t.walkableTile());
			//hallway and room are walkable
			t.setColor(Color.GRAY);
			assertTrue(t.walkableTile());
			t.setColor(Color.RED);
			assertTrue(t.walkableTile());
	}
	
	/**
	 * @author Kun Lin
	 */
	@Test public void tileColorChangeAsPlayerEnter(){
			Tile tile1 = new Tile (Color.GRAY,"");
			Tile tile2 = new Tile (Color.RED,"");
			tile1.enteringTile();
			tile2.enteringTile();
			/*tile1 is a hall way tile it should change color to black as a player enter
			* so other player can't enter the same tile
			* while tile2 is a room tile, multiple player can be in the same room at the same imte
			*/
			assertTrue(tile1.getColor().equals(Color.BLACK));
			assertTrue(tile2.getColor().equals(Color.RED));
	}
	/**
	 * @author Kun Lin
	 */
	@Test public void tileColorChangeAsPlayerLeaving(){
			Tile tile1 = new Tile (Color.BLACK,"");
			Tile tile2 = new Tile (Color.RED,"");
			tile1.leavingTile();
			tile2.leavingTile();
			//tile1 should change color since its a hall way tile
			//Tile2 should not change color since its a room tile 
			assertTrue(tile1.getColor().equals(Color.GRAY));
			assertTrue(tile2.getColor().equals(Color.RED));
	}
}
